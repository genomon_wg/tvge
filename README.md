# tVGE: Tiny Virtual Grid Engine

tVGEは、MPIプログラム内からのプロセス生成に制限があるシステムへの対応を目標とした、[Virtual Grid Engine (VGE)](https://github.com/SatoshiITO/VGE) の機能のサブセットを再実装した実証コードです。
オリジナルVGEに対して、masterプロセス側でのジョブ制御プロセス類の生成をすべて削除し、また、workerプロセス側でのタスク実行プロセスの生成をsubprocess.Popen()からos.system()に変更しています。

## 動作検証

富岳上で、Python 2.7.15を用いて検証しました。

## 制限事項

オリジナルVGEの機能のうち、GenomonPipelineを実行するための必要最低限の機能のみを実装してあります。
GenomonPipleline側のソースコードも、若干修正する必要があります。
インストーラがまだないため、本ディレクトリごと適当な場所にコピーしてください。

##  使用方法

設定ファイルは、vge.cnfをコピーして使用してください。
コメントアウトしてあるのがディフォルト値です。
必要があるパラメータのみコメントを外して、値を変更してください。
実行時に、設定ファイルのパスを環境変数`VGE_CONF`で指定します。
パラメータ値の変更がなければ、この環境変数の指定は不要です。

以下のジョブ投入スクリプトの抜粋では、本ディレクトリをコピーした場所をシェル変数`VGE_DIR`で指定しているものとします。
    
    export PYTHONPATH=$VGE_DIR:$PYTHONPATH
    export VGE_CONF=./my_vge.cnf
    
    $VGE_DIR/vge_server start &

    mpiexec -of-proc vge_mpi_log/log $VGE_DIR/vge_mpi &

    genomon_pipeline ...

    $VGE_DIR/vge_server stop

`vge_server stop`コマンドは、MPIプロセスvge_mpiを停止させた後、サーバvge_serverを停止させます。

## GenomonPipelineの修正

京/富岳移植版GenomonPipelineでtVGEを使用するには、ソースコードを以下のように修正する必要があります。

### run.py

以下の箇所をコメントアウト
 
    #from VGE.vge_conf import *
 
        ...

        #default_vge_conf = ""
        #default_vge_conf = os.getcwd() + "/vge.cfg"
        #if os.path.isfile(default_vge_conf):
        #    pass
        #elif "VGE_CONF" in os.environ:
        #    default_vge_conf_dir = os.environ["VGE_CONF"]
        #    default_vge_conf = default_vge_conf_dir + "/vge.cfg"
        #    if os.path.isfile(default_vge_conf):
        #       pass
        #vge_conf.read(default_vge_conf)
        #vge_conf_check()

### stage_task.py

VGEモジュールのimport箇所を修正

    #from VGE.vge_task import vge_task
    from vge.client import vge_task

### dna_resource/*.py

以下の行をコメントアウト

    #SGE_TASK_ID=`expr VGE_BULKJOB_ID + 1`

tVGEでは、キーワード`VGE_BULKJOB_ID`の展開に対応していません。
SGE_TASK_IDは、環境変数として値がセットされます。