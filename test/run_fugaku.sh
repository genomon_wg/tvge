#!/bin/bash -x
#PJM --rsc-list "rscunit=rscunit_ft01"
########PJM --rsc-list "rscgrp=eap-small"
#PJM --rsc-list "rscgrp=eap-llio"
#PJM --rsc-list "node=1"
#PJM --mpi "proc=4" 
#PJM --rsc-list "elapse=10:00"
#PJM -S

module load Python2-CN
export FLIB_CNTL_BARRIER_ERR=FALSE
export LD_PRELOAD=/usr/lib/FJSVtcs/ple/lib64/libpmix.so  # for mpi4py

VGE_DIR=..
export VGE_CONF=./vge.cnf

$VGE_DIR/vge_server start &

export PLE_MPI_STD_EMPTYFILE=off
mpiexec -of-proc vge_mpi_log/log $VGE_DIR/vge_mpi &

export PYTHONPATH=$VGE_DIR:$PYTHONPATH
./test_client.py

$VGE_DIR/vge_server stop
