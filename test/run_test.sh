#!/bin/bash

VGE_DIR=..
export VGE_CONF=./vge.cnf

echo 'start vge_server'
$VGE_DIR/vge_server start 2> vge_server_log &

echo 'start vge_mpi'
mpiexec -np 4 --output-filename vge_mpi_log $VGE_DIR/vge_mpi > /dev/null 2>&1 &

export PYTHONPATH=$VGE_DIR:$PYTHONPATH
echo 'run test_client.py...'
./test_client.py

echo 'shutdown...'
$VGE_DIR/vge_server stop
