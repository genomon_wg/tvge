#!/usr/bin/env python

import threading

from vge.client import vge_task

def run_express_job():
    command = '#/bin/bash\necho "express"'
    print 'job "test_express" start'
    ret = vge_task(command, 0, 'test_express', '')
    print 'job "test_express" finish, return code =', ret

print 'job "test" start'
test="""#/bin/bash
echo "SGE_TASK_ID = ${SGE_TASK_ID}" 1>&2
pwd
sleep 5
date 1>&2
"""
ret = vge_task(test, 0, 'test', '')
print 'job "test" finish, return code =', ret

threading.Timer(2.0, run_express_job).start()

print 'job "test_bulkjob" x 10 with "low_priority" start'
test_bulkjob = """#/bin/bash
echo "SGE_TASK_ID = ${SGE_TASK_ID}"
sleep 10
echo "stderr" 1>&2
"""
ret = vge_task(test_bulkjob, 10, 'test_bulkjob', '', 'low_priority')
print 'job "test_bulkjob" finish, return code =', ret

print 'job "test_pool" x 4 start'
test_pool = """#/bin/bash
echo "SGE_TASK_ID = ${SGE_TASK_ID}" 1>&2
./test_pool.py
"""
ret = vge_task(test_pool, 4, 'test_pool', '')
print 'job "test_pool" finish, return code =', ret

print 'job "test_popen" x 4 start'
test_popen = """#/bin/bash
echo "SGE_TASK_ID = ${SGE_TASK_ID}" 1>&2
./test_popen.py
"""
ret = vge_task(test_popen, 4, 'test_popen', '')
print 'job "test_popen" finish, return code =', ret
