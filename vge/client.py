import os
import socket
import sys
import time
import xmlrpclib

from vge.conf import conf

server_url = 'http://localhost:{:d}'.format(conf['server']['port'])
server_timeout = conf['client']['server_timeout']
interval_check_req_complete = conf['client']['interval_check_req_complete']


server = xmlrpclib.ServerProxy(server_url)

pid = os.getpid()

connection_checked = False

def check_server(server):
    global connection_checked
    connection_checked = True
    n = 0
    while True:
        try:
            server.nop()
            return True
        except socket.error:
            if n > server_timeout: return False
            time.sleep(1.0)
            n += 1


def ServerProxy():
    if connection_checked:
        return server
    else:
        if check_server(server):
            return server
        else: 
            raise socket.error


def wait_complete(req_id):
    while True:
        ret = server.query(req_id)
        if ret['status'] == 'done': return ret['return_code']
        time.sleep(interval_check_req_complete)


def vge_task(arg1, arg2, arg3, arg4, arg5=None):

    if not connection_checked:
        if not check_server(server):
            sys.stderr.write('error: cannot connect to server\n')
            sys.exit(1)

    req = {
        'command': arg1,
        'basefilename': arg3,
        'bulkjob': False,
        'n_job': 1,
        'client_pid': pid,
        'priority': 'normal'
    }
    max_task = arg2
    if max_task > 0:
        req['bulkjob'] = True
        req['n_job'] = max_task
    if arg5 =='low_priority':
        req['priority'] = 'low'

    req_id = server.submit(req)

    ret = wait_complete(req_id)

    return ret
