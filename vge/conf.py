import ConfigParser
import logging
import os
import sys


default_config_file = os.path.dirname(__file__) + '/default.cfg'


options = {
    'server': {
        'port' : 'int',
        'script_dir': 'str',
        'job_list_file': 'str',
        'req_list_file': 'str',
        'logging_level': 'logging_level',
    },
    'client': {
        'server_timeout': 'int',
        'interval_check_req_complete': 'float',
        'logging_level': 'logging_level',
    },
    'mpi_master': {
        'max_job_get': 'int',
        'logging_level': 'logging_level',
    },
    'mpi_worker': {
        'interval_check_new_job': 'float',
        'logging_level': 'logging_level',
    }
}


logging_level_tab = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL,
}


def get_options(config, sec, opt, type):
    if type == 'int':
        return config.getint(sec, opt)
    elif type == 'float':
        return config.getfloat(sec, opt)
    elif type == 'bool':
        return config.getboolean(sec, opt)
    elif type == 'str':
        return config.get(sec, opt)
    elif type == 'logging_level':
        return logging_level_tab[config.get(sec, opt)]
    else:
        raise TypeError(type)


def read_default_conf(filename):
    conf = {}
    config = ConfigParser.ConfigParser()
    config.readfp(open(filename))
    for sec in options.keys():
        conf[sec] = {}
        for opt, type in options[sec].items():
            conf[sec][opt] = get_options(config, sec, opt, type)
    return conf


def read_user_conf(conf, filename):
    config = ConfigParser.ConfigParser()
    try:
        config.readfp(open(filename))
    except IOError as e:
        sys.stderr.write('*** error: {}: {}, ignored\n'.format(e.strerror, filename))
        return conf
    for sec in config.sections():
        if not sec in options:
            sys.stderr.write('*** warning: invalid section "{}" in file "{}", ignored\n'
                             .format(sec, filename))
            continue
        for opt, val in config.items(sec):
            if not opt in options[sec]:
                sys.stderr.write('*** warning: invalid option "{}" at secton "{}" in file "{}", ' \
                                 'ignored\n'.format(opt, sec, filename))
                continue
            try:
                conf[sec][opt] = get_options(config, sec, opt, options[sec][opt])
            except:
                sys.stderr.write('*** warning: invalid value "{}" for option "{}" ' \
                                 'at secton "{}" in file "{}", ignored\n'
                                 .format(val, opt, sec, filename))
                continue
    return conf


conf = read_default_conf(default_config_file)
#sys.stderr.write('conf=' + str(conf) + '\n')

user_config_file = os.getenv('VGE_CONF')
if user_config_file:
    sys.stderr.write('read user config file: {}\n'.format(user_config_file))
    conf = read_user_conf(conf, user_config_file)

sys.stderr.write('conf=' + str(conf) + '\n')


if __name__ == '__main__':

    conf = read_default_conf(default_config_file)
    print conf

    conf = read_user_conf(conf, './user.cfg')
    print conf

