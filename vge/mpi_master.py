import logging
import socket
import time
from collections import deque

from mpi4py import MPI

import vge.client
import vge.node_info
from vge.conf import conf


logger = logging.getLogger(__name__)
logger.setLevel(conf['mpi_master']['logging_level'])
logging.basicConfig(format='%(asctime)s:%(levelname)s:VGE[%(module)s] %(message)s')

max_job_get = conf['mpi_master']['max_job_get']

TAG_REQUEST_NEW_JOB = 10
TAG_NEW_JOB = 20


def run():

    comm = MPI.COMM_WORLD
    status = MPI.Status()

    logger.info('start master on [rank:{:d}, hostname:{:}, node_id:{:}]'
                .format(comm.Get_rank(), MPI.Get_processor_name(), vge.node_info.get_node_id()))

    server_connected = False
    is_shutdown = False
    shutdown_num_workers = 0
    shutdown_msg = {'job_id': -1}

    job_list = deque([])
    num_workers = comm.Get_size() - 1

    try:
        server = vge.client.ServerProxy()
        server_connected = True
    except socket.error:
        logger.critical('cannot connect to server')
        logger.critical('shutdown...')
        is_shutdown = True

    while True:
        run_info = comm.recv(source=MPI.ANY_SOURCE, tag=TAG_REQUEST_NEW_JOB, status=status)
        rank = status.Get_source()
        logger.debug('recv run_info [{:}] form worker [{:d}]'.format(str(run_info), rank))

        if run_info:
            if run_info['job_id'] == -1:
                # worker process shutted down
                shutdown_num_workers += 1
                if shutdown_num_workers == num_workers:
                    if server_connected:
                        server.put_run_info(run_info)
                    break
                continue
            else:
                # normal job completed
                run_info['worker'] = rank
                server.put_run_info(run_info)

        if is_shutdown:
            comm.send(shutdown_msg, dest=rank, tag=TAG_NEW_JOB)
            logger.info('send shutdown msg to worker[{:d}]'.format(rank))
            continue

        if not job_list:
            if max_job_get > 0:
                new_job_list = server.get_new_jobs(max_job_get)
            else:
                new_job_list = server.get_new_jobs()
            if new_job_list:
                logger.info('get {:d} new job(s)'.format(len(new_job_list)))
                job_list.extend(new_job_list)

        if job_list:
            job = job_list.popleft()
            if job['job_id'] == -1:
                is_shutdown = True
                comm.send(shutdown_msg, dest=rank, tag=TAG_NEW_JOB)
                logger.info('send shutdown msg to worker[{:d}]'.format(rank))
                continue
        else:
            job = None

        comm.send(job, dest=rank, tag=TAG_NEW_JOB)

    logger.info('shutdown')
