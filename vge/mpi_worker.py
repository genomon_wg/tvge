import logging
import os
import time
from collections import deque

from mpi4py import MPI

import vge.node_info
from vge.conf import conf


logger = logging.getLogger(__name__)
logger.setLevel(conf['mpi_worker']['logging_level'])
logging.basicConfig(format='%(asctime)s:%(levelname)s:VGE[%(module)s] %(message)s')

interval_check_new_job = conf['mpi_worker']['interval_check_new_job']

TAG_REQUEST_NEW_JOB = 10
TAG_NEW_JOB = 20


def run():

    comm = MPI.COMM_WORLD

    logger.info('start worker on [rank:{:d}, hostname:{:}, node_id:{:}]'
                .format(comm.Get_rank(), MPI.Get_processor_name(), vge.node_info.get_node_id()))
    
    run_info = None

    while True:
        comm.send(run_info, dest=0, tag=TAG_REQUEST_NEW_JOB)

        job = comm.recv(source=0, tag=TAG_NEW_JOB)
        logger.debug('recv job [{:}]'.format(str(job)))

        if not job:
            run_info = None
            time.sleep(interval_check_new_job)
            continue

        if job['job_id'] == -1:
            # shutdown worker process
            #msg = {'run_info': {'job_id': -1}}
            #comm.send(msg, dest=0, tag=TAG_REQUEST_NEW_JOB)
            run_info = {'job_id': -1}
            comm.send(run_info, dest=0, tag=TAG_REQUEST_NEW_JOB)
            break

        run_info = execute_job(job)

    logger.info('shutdown')


def execute_job(job):
    job_id, bulkjob_id, filename = job['job_id'], job['bulkjob_id'], job['filename']
    stdout = '{}.{:d}.o{:d}'.format(filename, bulkjob_id, job_id)
    stderr = '{}.{:d}.e{:d}'.format(filename, bulkjob_id, job_id)
    command = 'env SGE_TASK_ID={:d} {} > {} 2> {}'.format(bulkjob_id + 1, filename, stdout, stderr)
    basename = os.path.basename(filename)
    logger.info('job [job_id:{:d}, bulkjob_id:{:d}, "{}"] start'
                .format(job_id, bulkjob_id, basename))

    start_time = time.time()
    ret = os.system(command)
    finish_time = time.time()

    return_code = ret >> 8
    signal_no = ret & 0xFF
    logger.info('job [job_id:{:d}, bulkjob_id:{:d}, "{}"] return with [{:d}, {:d}]'
                .format(job_id, bulkjob_id, basename, return_code, signal_no))
    run_info = {
        'job_id': job_id,
        'return_code': return_code,
        'start_time': start_time,
        'finish_time': finish_time
    }

    return run_info
