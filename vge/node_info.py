# K
#node_info_file = '/etc/opt/FJSVpnavi/psm/cluster.d/mynodeinfo' 

# Fugaku
node_info_file = '/etc/opt/FJSVtcs/psm/cluster_config/cluster.d/mynodeinfo'


def get_node_info():
    cluster_id = 'unknown'
    node_id = 'unknown'
    try:
        with open(node_info_file) as f:
            for line in f:
                items = line.split()
                if items[0] == 'ClusterID': cluster_id = items[2][1:-1]
                if items[0] == 'NodeID': node_id = items[2][1:-1]
    except:
        pass
    return cluster_id, node_id


def get_node_id():
    cluster_id, node_id = get_node_info()
    return node_id


if __name__ == '__main__':

    cluster_id, node_id = get_node_info()

    print('cluster_id = {}'.format(cluster_id))
    print('node_id = {}'.format(node_id))

    print('node_id = {}'.format(get_node_id()))
