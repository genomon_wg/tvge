import logging
import os
import threading
import time
from collections import deque
from SimpleXMLRPCServer import SimpleXMLRPCServer

from vge.conf import conf


logger = logging.getLogger(__name__)
logger.setLevel(conf['server']['logging_level'])
logging.basicConfig(format='%(asctime)s:%(levelname)s:VGE[%(module)s] %(message)s')


script_dir = conf['server']['script_dir']
server_port = conf['server']['port']
job_list_file = script_dir + '/' + conf['server']['job_list_file']
req_list_file = script_dir + '/' + conf['server']['req_list_file']


class JobController:

    def __init__(self, server):
        self._server = server 
        self._req_list = []
        self._job_list = []
        self._job_queue = deque([])
        self._low_priority_job_queue = deque([])
        self._num_req = 0
        self._num_job = 0
        self._shutdown = False
        self._mpi_shutted_down = False

    def submit(self, req):
        req['submit_time'] = time.time()
        req_id = self._num_req
        self._num_req += 1
        req['req_id'] = req_id
        req['status'] = 'submitted'
        self._req_list.append(req)

        if req['bulkjob']:
            req['n_done'] = 0
            req['return_code'] = 0
            for bulkjob_id in range(req['n_job']):
                self._register_new_job(req, req_id, bulkjob_id)
            logger.info('submit bulkjob [req_id:{:d}, "{}" x {:d}, priority:{}] from pid:{:d}'
                        .format(req_id, req['basefilename'], req['n_job'], req['priority'],
                                req['client_pid']))
        else:
            self._register_new_job(req, req_id, bulkjob_id=0)
            logger.info('submit job [req_id:{:d}, "{}", priority:{}] from pid:{:d}'
                        .format(req_id, req['basefilename'], req['priority'],
                        req['client_pid']))
        del req['command']
        return req_id

    def _register_new_job(self, req, req_id, bulkjob_id):
        job_id = self._num_job
        self._num_job += 1
        filename = '{}/{}.{:d}.sh'.format(script_dir, req['basefilename'], req_id)
        if bulkjob_id == 0:
            with open(filename, 'w') as f:
                f.write(req['command']) 
            os.chmod(filename, 0744)
        job = {
            'job_id': job_id,
            'bulkjob_id': bulkjob_id,
            'req_id': req_id,
            'filename': filename,
        }
        self._job_list.append(job)
        if req['priority'] ==  'low':
            self._low_priority_job_queue.append(job_id)
        else:
            self._job_queue.append(job_id)

    def query(self, req_id):
        if req_id == -1:
            #shutdown request
            if self._mpi_shutted_down:
                self._write_lists()
                t = threading.Timer(2.0, self._server.shutdown)
                t.start()
                logger.info('shutdown server')
                return {'status': 'done', 'return_code':0}
            elif self._shutdown:
                return {'status': 'submitted'}
            else:
                return {'status': 'not_submitted'}
        ret = {}
        try:
            ret['status'] = self._req_list[req_id]['status']
            if ret['status'] == 'done':
                ret['return_code'] = self._req_list[req_id]['return_code']
        except IndexError:
            ret['status'] = 'not_submitted'
        return ret

    def get_new_jobs(self, max_job=None):
        if self._shutdown:
            return [{'job_id': -1, 'bulkjob_id': 0, 'filename': '*SHUTDOWN*'}]
        ret = []
        n = len(self._job_queue) + len(self._low_priority_job_queue)
        if max_job: n = min(n, max_job)
        while len(self._job_queue) and n:
            n -= 1
            ret.append(self._pop_job(self._job_queue))
        while len(self._low_priority_job_queue) and n:
            n -= 1
            ret.append(self._pop_job(self._low_priority_job_queue))
        return ret

    def _pop_job(self, job_queue):
        job_id = job_queue.popleft()
        job = self._job_list[job_id]
        return {
            'job_id': job_id,
             'bulkjob_id': job['bulkjob_id'],
             'filename': job['filename'],
        }

    def put_run_info(self, run_info):
        job_id = run_info['job_id']
        if job_id == -1:
            # mpi shutdown
            self._mpi_shutted_down = True
            return True
        job = self._job_list[job_id]
        for item in ['return_code', 'start_time', 'finish_time', 'worker']:
            job[item] = run_info[item]
        req_id = job['req_id']
        req = self._req_list[req_id]
        if req['bulkjob']:
            req['n_done'] += 1
            # for bulk job, req's return code is 0 or last non-zero return code if exist
            if run_info['return_code'] != 0: req['return_code'] = run_info['return_code']
            if req['n_done'] == req['n_job']:
                req['finish_time'] = time.time()
                req['status'] = 'done'
                logger.info('complete bulkjob [req_id:{:d}, "{}" x {:d}] with return_code:{:d}'
                            .format(req_id, req['basefilename'], req['n_job'], req['return_code']))
        else:
            req['finish_time'] = time.time()
            req['status'] = 'done'
            req['return_code'] = run_info['return_code']
            logger.info('complete job [req_id:{:d}, "{}"] with return_code:{:d}'
                        .format(req_id, req['basefilename'], req['return_code']))
        return True

    def shutdown(self):
        logger.info('shutdown server...')
        self._shutdown = True
        req_id = -1
        return req_id

    def nop(self):
        return True

    def _write_lists(self):
        import csv
        from datetime import datetime

        with open(job_list_file, 'w') as csvfile:
            fields = ['job_id', 'bulkjob_id', 'req_id', 'start_time', 'finish_time',
                      'elapsed_time', 'filename', 'return_code', 'worker']
            writer = csv.DictWriter(csvfile, fieldnames=fields, extrasaction='ignore')
            writer.writeheader()
            for job in self._job_list:
                job['elapsed_time'] = job['finish_time'] - job['start_time']
                job['start_time'] = datetime.fromtimestamp(job['start_time'])
                job['finish_time'] = datetime.fromtimestamp(job['finish_time'])
                job['filename'] = os.path.basename(job['filename'])
                writer.writerow(job)

        with open(req_list_file, 'w') as csvfile:
            fields = ['req_id', 'status', 'bulkjob', 'n_job', 'n_done', 'return_code',
                       'basefilename', 'submit_time', 'finish_time', 'client_pid', 'priority']
            writer = csv.DictWriter(csvfile, fieldnames=fields, extrasaction='ignore')
            writer.writeheader()
            for req in self._req_list:
                req['submit_time'] = datetime.fromtimestamp(req['submit_time'])
                req['finish_time'] = datetime.fromtimestamp(req['finish_time'])
                writer.writerow(req)


def start_server():
    if not os.path.isdir(script_dir): os.mkdir(script_dir)

    server = SimpleXMLRPCServer(("localhost", server_port), logRequests=False)
    server.register_instance(JobController(server), allow_dotted_names=True)
    server.serve_forever()


if __name__ == '__main__':

    start_server()
